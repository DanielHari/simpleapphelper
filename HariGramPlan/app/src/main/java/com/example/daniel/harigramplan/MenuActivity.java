package com.example.daniel.harigramplan;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity {


    LinearLayout facebook,instagram,twitter,tesla,amazon,MarketPlace,weather,europass,personalData,camera;
    ImageView addPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        facebook = (LinearLayout)findViewById(R.id.LinearLayoutfacebook);
        instagram = (LinearLayout)findViewById(R.id.LinearLayoutInstagram);
        twitter = (LinearLayout)findViewById(R.id.LinearLayoutTwitter);
        tesla = (LinearLayout)findViewById(R.id.LinearLayoutTesla);
        amazon = (LinearLayout)findViewById(R.id.LinearLayoutAmazon);
        MarketPlace = (LinearLayout)findViewById(R.id.LayoutLinearMarketPlace);
        weather = (LinearLayout)findViewById(R.id.LayoutLinearWeather);
        europass = (LinearLayout)findViewById(R.id.LayoutLinearEuropass);
        personalData = (LinearLayout)findViewById(R.id.LayoutLinearPersonalData);
        camera = (LinearLayout)findViewById(R.id.LinearLayoutCamera);
        addPhoto = (ImageView)findViewById(R.id.addPhoto);


        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addPhoto = new Intent(
                        MenuActivity.this, AddPhoto.class);
                startActivity(addPhoto);
            }
        });


        personalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Personaldata = new Intent(
                        MenuActivity.this, PersonalData.class);
                startActivity(Personaldata);
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Personaldata = new Intent(
                        MenuActivity.this, Camera.class);
                startActivity(Personaldata);
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://sl-si.facebook.com/"));
                startActivity(browserIntent);
            }
        });

        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.instagram.com/"));
                startActivity(browserIntent);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://twitter.com/"));
                startActivity(browserIntent);
            }
        });

        tesla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.tesla.com/"));
                startActivity(browserIntent);
            }
        });

        amazon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.amazon.com/"));
                startActivity(browserIntent);
            }
        });

        MarketPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.finsky&hl=en"));
                startActivity(browserIntent);
            }
        });

        weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.accuweather.com/sl/si/ljubljana/299198/weather-forecast/299198"));
                startActivity(browserIntent);
            }
        });

        europass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://europass.cedefop.europa.eu/editors/en/cv/compose"));
                startActivity(browserIntent);
            }
        });


    }
}
